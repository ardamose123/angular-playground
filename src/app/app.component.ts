import {Component} from "@angular/core";
import {OAuthService} from "angular-oauth2-oidc";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.sass"],
})
export class AppComponent {
  title = "generic-table";

  constructor(private oauthService: OAuthService) {
    // URL of the SPA to redirect the user to after login
    this.oauthService.redirectUri = window.location.origin + "/";

    // The SPA's id. The SPA is registered with this id at the auth-server
    this.oauthService.clientId = "frontend";

    // set the scope for the permissions the client should request
    // The first three are defined by OIDC.
    this.oauthService.scope = "openid profile email";

    // set to true, to receive also an id_token via OpenId Connect (OIDC) in addition to the
    // OAuth2-based access_token
    this.oauthService.oidc = true; // ID_Token

    // Use setStorage to use sessionStorage or another implementation of the TS-type Storage
    // instead of localStorage
    this.oauthService.setStorage(localStorage);

    // Discovery Document of your AuthServer as defined by OIDC
    const url = "http://localhost:8082/auth/realms/sandbox/.well-known/openid-configuration";

    // Load Discovery Document and then try to log in the user
    this.oauthService.loadDiscoveryDocument(url)
      .then(() => {
        // This method just tries to parse the token(s) within the url when
        // the auth-server redirects the user back to the web-app
        // It doesn't send the user the login page
        this.oauthService.tryLogin({});
      });
  }

  goHome() {
    document.location.href = "/";
  }
}
