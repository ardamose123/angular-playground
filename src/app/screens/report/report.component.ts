import {ChangeDetectionStrategy, Component, OnInit} from "@angular/core";

@Component({
  selector: "app-report",
  templateUrl: "./report.component.html",
  styleUrls: ["./report.component.sass"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReportComponent implements OnInit {

  sampleData = {
    producers: [{
      id: 123,
      registryId: 10002000345,
      classifier: 4500,
      name: "Company ABC",
      totalToPay: 5000.00,
      notYetPaidGoods: 5,
      goodsInQuarter: 20,
      budgetInQuarter: 10,
      overshoot: 15,
    }, {
      id: 124,
      registryId: 10002000400,
      classifier: 4500,
      name: "Company XYZ",
      totalToPay: 345.00,
      notYetPaidGoods: 0,
      goodsInQuarter: 355,
      budgetInQuarter: 700,
      overshoot: 0,
    }],
  };

  constructor() {
  }

  ngOnInit(): void {
  }

  getData() {
    console.count("Get data");
    return this.sampleData;
  }
}
