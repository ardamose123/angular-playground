import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {HomeComponent} from "./home/home.component";
import {RouterModule} from "@angular/router";
import {ReportComponent} from "./report/report.component";
import {MatExpansionModule} from "@angular/material/expansion";
import {DataTypeRenderingModule} from "../data-type-rendering/data-type-rendering.module";
import {MatButtonModule} from "@angular/material/button";


@NgModule({
  declarations: [
    HomeComponent,
    ReportComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatExpansionModule,
    DataTypeRenderingModule,
    MatButtonModule,
  ],
  exports: [
    HomeComponent,
    ReportComponent,
  ],
})
export class ScreensModule {
}
