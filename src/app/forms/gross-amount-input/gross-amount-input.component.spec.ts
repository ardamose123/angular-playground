import {ComponentFixture, TestBed} from "@angular/core/testing";

import {GrossAmountInputComponent} from "./gross-amount-input.component";

describe("GrossAmountInputComponent", () => {
  let component: GrossAmountInputComponent;
  let fixture: ComponentFixture<GrossAmountInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [GrossAmountInputComponent],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GrossAmountInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component)
      .toBeTruthy();
  });
});
