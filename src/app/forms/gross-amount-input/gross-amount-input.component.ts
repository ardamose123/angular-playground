import {Component, ElementRef, Input, OnInit, Optional, Self} from "@angular/core";
import {FormGroup, NgControl} from "@angular/forms";
import {MatFormFieldControl} from "@angular/material/form-field";
import {combineLatest, Observable} from "rxjs";
import {ConstantFormControl} from "../basics/constant-form-control";

@Component({
  selector: "app-gross-amount-input",
  templateUrl: "./gross-amount-input.component.html",
  styleUrls: ["./gross-amount-input.component.sass"],
  providers: [{provide: MatFormFieldControl, useExisting: GrossAmountInputComponent}],
})
export class GrossAmountInputComponent extends ConstantFormControl<number> implements OnInit {
  @Input() formGroup: FormGroup;
  @Input() netAmountField: string;
  @Input() vatRateField: string;

  constructor(
    @Optional() @Self() public ngControl: NgControl,
    protected elementRef: ElementRef<HTMLElement>,
  ) {
    super(ngControl, elementRef);
  }

  ngOnInit(): void {
    const netAmount$ = this.getFieldValueChanges(this.netAmountField);
    const vatRate$ = this.getFieldValueChanges(this.vatRateField);

    const valueChangeSubscription = combineLatest([netAmount$, vatRate$])
      .subscribe(i => {
        const n = parseFloat(i[0]);
        const r = parseFloat(i[1]);

        if (isNaN(n) || isNaN(r)) {
          this.errorState = true;
          this.value = null;
        } else {
          this.errorState = false;
          this.value = n * (100 + r) * 0.01;
        }
      });

    this.subscriptions.push(valueChangeSubscription);
  }

  private getFieldValueChanges(fieldName: string): Observable<any> {
    const field = this.formGroup.get(fieldName);
    if (field) {
      return field.valueChanges;
    } else {
      throw new Error(`Field ${fieldName} is not part of this control's form.`);
    }
  }
}
