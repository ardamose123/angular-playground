import {
  AfterViewInit,
  Component,
  ElementRef,
  HostBinding,
  Input,
  OnDestroy,
  Optional,
  Self,
  ViewChild,
} from "@angular/core";
import {ControlValueAccessor, NgControl} from "@angular/forms";
import {MatFormFieldControl} from "@angular/material/form-field";
import {Subject, Subscription} from "rxjs";

@Component({
  selector: "app-amount",
  templateUrl: "./amount.component.html",
  styleUrls: ["./amount.component.sass"],
  providers: [{provide: MatFormFieldControl, useExisting: AmountComponent}],
})
export class AmountComponent implements AfterViewInit, OnDestroy, MatFormFieldControl<number>, ControlValueAccessor {
  @Input() placeholder: string;
  @Input() disabled: boolean;
  @Input() required: boolean;

  @HostBinding("id") id: string;
  @HostBinding("class.floating") shouldLabelFloat: boolean;

  @ViewChild("theInput") theInput: ElementRef;

  focused = false;
  empty = true;
  errorState = false;
  controlType = "constant";
  autofilled = true;
  userAriaDescribedBy = undefined;

  stateChanges = new Subject<void>();
  subscriptions: Subscription[] = [];

  private internalValue: number | null;

  constructor(
    @Optional() @Self() public ngControl: NgControl,
  ) {
    ngControl.valueAccessor = this;
  }

  get value(): number | null {
    return this.internalValue;
  }

  onChange = (_: any) => {
  };

  onTouched = () => {
  };

  public setValue(input: any): void {
    this.empty = typeof input !== "string" || input === "";
    if (this.empty) {
      this.internalValue = null;
      this.shouldLabelFloat = false;
    } else {
      this.shouldLabelFloat = true;

      const stringInput = input as string;
      const parsedValue = parseFloat(stringInput);
      this.errorState = isNaN(parsedValue);

      if (this.errorState) {
        this.internalValue = null;
      } else {
        this.internalValue = Math.round(parsedValue * 100) * 0.01;
      }
    }

    this.onChange(this.value);
    this.onTouched();
    this.stateChanges.next();
  }

  ngAfterViewInit(): void {
    this.theInput.nativeElement.value = this.value;
  }

  setDescribedByIds(ids: string[]): void {
    // Do nothing
  }

  onContainerClick(event: MouseEvent): void {
    // Do nothing
  }

  writeValue(obj: any): void {
    this.setValue(obj);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;

    // Since registerOnChange is called after writeValue,
    // we need to call this function in order to feed the form with the initial value as calculated by this.setValue().
    this.onChange(this.value);
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  ngOnDestroy() {
    this.stateChanges.complete();
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  focus() {
    this.focused = true;
    this.shouldLabelFloat = true;
    this.theInput.nativeElement.focus();
  }

  blur() {
    this.focused = false;

    const rawValue = this.theInput.nativeElement.value;
    this.setValue(rawValue);
  }
}
