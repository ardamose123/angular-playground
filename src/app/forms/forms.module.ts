import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {FormsModule as ANgularFormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {DataTypeRenderingModule} from "../data-type-rendering/data-type-rendering.module";
import {AmountComponent} from "./amount/amount.component";
import {GrossAmountInputComponent} from "./gross-amount-input/gross-amount-input.component";
import {SampleFormComponent} from "./sample-form/sample-form.component";

@NgModule({
  declarations: [
    SampleFormComponent,
    GrossAmountInputComponent,
    AmountComponent,
  ],
  imports: [
    CommonModule,
    ANgularFormsModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    ReactiveFormsModule,
    DataTypeRenderingModule,
  ],
  exports: [
    SampleFormComponent,
    GrossAmountInputComponent,
    AmountComponent,
  ],
})
export class FormsModule {
}
