import {ElementRef, HostBinding, Injectable, Input, OnDestroy, Optional, Self} from "@angular/core";
import {ControlValueAccessor, NgControl} from "@angular/forms";
import {MatFormFieldControl} from "@angular/material/form-field";
import {Subject, Subscription} from "rxjs";

@Injectable()
export class ConstantFormControl<T> implements OnDestroy, MatFormFieldControl<T>, ControlValueAccessor {
  @Input() placeholder: string;

  @HostBinding("id") id: string;
  @HostBinding("class.floating") shouldLabelFloat = true;

  focused = false;
  empty = true;
  required = true;
  disabled = true;
  errorState = false;
  controlType = "constant";
  autofilled = true;
  userAriaDescribedBy = undefined;

  stateChanges = new Subject<void>();
  subscriptions: Subscription[] = [];

  private internalValue: T | null;

  constructor(
    @Optional() @Self() public ngControl: NgControl,
    protected elementRef: ElementRef<HTMLElement>,
  ) {
    ngControl.valueAccessor = this;
  }

  get value(): T | null {
    return this.errorState ? null : this.internalValue;
  }

  set value(newValue: T | null) {
    this.internalValue = newValue;
    this.onChange(newValue);
    this.stateChanges.next();
  }

  onChange = (_: any) => {
  };

  setDescribedByIds(ids: string[]): void {
    // Do nothing
  }

  onContainerClick(event: MouseEvent): void {
    // Do nothing
  }

  writeValue(obj: any): void {
    // Do nothing. Value is independent from user input or any default values.
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
    this.onChange(this.value);
  }

  registerOnTouched(fn: any): void {
    // Do nothing.
  }

  ngOnDestroy() {
    this.stateChanges.complete();
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
