import {Component, OnInit} from "@angular/core";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from "@angular/material/form-field";

@Component({
  selector: "app-sample-form",
  templateUrl: "./sample-form.component.html",
  styleUrls: ["./sample-form.component.sass"],
  providers: [
    {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {appearance: "standard", floatLabel: "auto"}},
  ],
})
export class SampleFormComponent implements OnInit {
  theForm: FormGroup;

  constructor(formBuilder: FormBuilder) {
    this.theForm = formBuilder.group({
      netAmount: [null, [Validators.required, Validators.min(0)]],
      vatRate: [""],
      grossAmount: [""],
      theConst: [],
    });
  }

  ngOnInit(): void {

  }

  logFormValue() {
    console.log("Form", this.theForm.value);
    console.log("Const", this.theForm.get("theConst")?.value);
    console.log("Is valid?", this.theForm.valid);
    console.log("Net amount errors", this.theForm.get("netAmount")?.errors);
    return false;
  }
}
