export interface Company {
  id: UUID;
  name: string;
  address: Address;
  bankAccount: BankAccount;
  iban: string;
}

export interface Address {
  street: string;
  number: string;
  city: string;
  zipCode: string;
  country: string;
}

export interface BankAccount {
  iban: string;
  bic: string;
  holder: string;
}

export type UUID = string;
