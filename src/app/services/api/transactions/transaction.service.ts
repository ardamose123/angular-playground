import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Company} from "./dtos/company";

@Injectable({
  providedIn: "root",
})
export class TransactionService {

  private url = "http://localhost:8080";

  constructor(private api: HttpClient) {
  }

  listCompanies(): Observable<Company[]> {
    return this.api.get<any[]>(this.url + "/companies");
  }

  upsertCompany(company: Company): Observable<void> {
    return this.api.post<void>(this.url + "/companies", company);
  }
}
