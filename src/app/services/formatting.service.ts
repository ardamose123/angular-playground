import {Injectable} from "@angular/core";

type Cache<T> = { [key: string]: T };

@Injectable({
  providedIn: "root",
})
export class FormattingService {
  language: "en";
  locale = "de-DE";
  currencyFormatCache: Cache<Intl.NumberFormat> = {};
  currencyFormatConfig = {
    style: "currency",
    currencyDisplay: "narrowSymbol",
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
    useGrouping: true,
  };
  percentFormatCache: Cache<Intl.NumberFormat> = {};
  percentFormatConfig = {
    style: "percent",
    minimumFractionDigits: 2,
    maximumFractionDigits: 2,
  };
  dateFormatCache: Intl.DateTimeFormat;
  dateFormatConfig: Intl.DateTimeFormatOptions = {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
    timeZone: "UTC",
  };
  dateTimeFormatCache: Intl.DateTimeFormat;
  dateTimeFormatConfig: Intl.DateTimeFormatOptions = {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
    hour: "2-digit",
    minute: "2-digit",
    second: "2-digit",
  };

  constructor() {
  }

  currencyFormat(currency: string = "EUR"): Intl.NumberFormat {
    return this.currencyFormatCache[currency] = this.currencyFormatCache[currency] || Intl.NumberFormat(this.locale, {
      ...this.currencyFormatConfig,
      currency,
    });
  }

  percentFormat(precision: number): Intl.NumberFormat {
    return this.percentFormatCache[precision] = this.percentFormatCache[precision] || Intl.NumberFormat(this.locale, {
      ...this.percentFormatConfig,
      minimumFractionDigits: precision,
      maximumFractionDigits: precision,
    });
  }

  dateFormat(): Intl.DateTimeFormat {
    return this.dateFormatCache = this.dateFormatCache || Intl.DateTimeFormat(this.locale, this.dateFormatConfig);
  }

  dateTimeFormat(): Intl.DateTimeFormat {
    return this.dateTimeFormatCache = this.dateTimeFormatCache || Intl.DateTimeFormat(this.locale, this.dateTimeFormatConfig);
  }
}
