import {Pipe, PipeTransform} from "@angular/core";
import {FormattingService} from "../services/formatting.service";

@Pipe({
  name: "datetime",
})
export class DatetimePipe implements PipeTransform {
  format: Intl.DateTimeFormat;

  constructor(formats: FormattingService) {
    this.format = formats.dateTimeFormat();
  }

  transform(value: string): string {
    return this.format.format(Date.parse(value));
  }
}
