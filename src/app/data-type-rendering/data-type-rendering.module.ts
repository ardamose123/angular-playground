import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {PercentPipe} from "./percent.pipe";
import {AmountPipe} from "./amount.pipe";
import {AddressComponent} from "./address/address.component";
import {FormattingService} from "../services/formatting.service";
import {DatePipe} from "./date.pipe";
import {DatetimePipe} from "./datetime.pipe";

@NgModule({
  declarations: [PercentPipe, AmountPipe, AddressComponent, DatePipe, DatetimePipe],
  imports: [
    CommonModule,
  ],
  exports: [PercentPipe, AmountPipe, AddressComponent, DatePipe, DatetimePipe],
  providers: [FormattingService],
})
export class DataTypeRenderingModule {
}
