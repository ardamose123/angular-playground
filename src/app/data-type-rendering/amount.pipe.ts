import {Pipe, PipeTransform} from "@angular/core";
import {FormattingService} from "../services/formatting.service";

@Pipe({
  name: "amount",
})
export class AmountPipe implements PipeTransform {
  format: Intl.NumberFormat;

  constructor(formats: FormattingService) {
    this.format = formats.currencyFormat();
  }

  transform(value: number | null): string {
    return value === null ? "" : this.format.format(value);
  }
}
