import {Component, Input, OnInit} from "@angular/core";

@Component({
  selector: "app-address",
  templateUrl: "./address.component.html",
  styleUrls: ["./address.component.sass"],
})
export class AddressComponent implements OnInit {
  @Input() street: string;
  @Input() number: string;
  @Input() city: string;
  @Input() zipCode: string;
  @Input() country: string;
  @Input() source: any;
  @Input() minimized = false;

  constructor() {
  }

  ngOnInit(): void {
    if (!!this.source) {
      this.street = this.source.street;
      this.number = this.source.number;
      this.city = this.source.city;
      this.zipCode = this.source.zipCode;
      this.country = this.source.country;
    }
  }
}
