import {Pipe, PipeTransform} from "@angular/core";
import {FormattingService} from "../services/formatting.service";

@Pipe({
  name: "date",
})
export class DatePipe implements PipeTransform {
  format: Intl.DateTimeFormat;

  constructor(formats: FormattingService) {
    this.format = formats.dateFormat();
  }

  transform(value: string): string {
    return this.format.format(Date.parse(value));
  }
}
