import {Pipe, PipeTransform} from "@angular/core";
import {FormattingService} from "../services/formatting.service";

@Pipe({
  name: "percent",
})
export class PercentPipe implements PipeTransform {
  format: Intl.NumberFormat;

  constructor(formats: FormattingService) {
    this.format = formats.percentFormat(2);
  }

  transform(value: number): string {
    return this.format.format(value / 100);
  }

}
