import {NgModule} from "@angular/core";
import {MatToolbarModule} from "@angular/material/toolbar";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {FormsModule} from "./forms/forms.module";
import {GenericTableModule} from "./generic-table/generic-table.module";
import {ScreensModule} from "./screens/screens.module";
import {AnimationSandboxModule} from "./animation-sandbox/animation-sandbox.module";
import {HttpClientModule} from "@angular/common/http";
import {OAuthModule} from "angular-oauth2-oidc";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    GenericTableModule,
    MatToolbarModule,
    FormsModule,
    ScreensModule,
    AnimationSandboxModule,
    HttpClientModule,
    OAuthModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
}
