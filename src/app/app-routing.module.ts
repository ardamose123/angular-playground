import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {GenericTableComponent} from "./generic-table/generic-table.component";
import {SampleFormComponent} from "./forms/sample-form/sample-form.component";
import {HomeComponent} from "./screens/home/home.component";
import {ReportComponent} from "./screens/report/report.component";
import {SandboxComponent} from "./animation-sandbox/sandbox/sandbox.component";

const routes: Routes = [
  {path: "", component: HomeComponent},
  {path: "table", component: GenericTableComponent},
  {path: "form", component: SampleFormComponent},
  {path: "report", component: ReportComponent},
  {path: "animations", component: SandboxComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {relativeLinkResolution: "legacy"})],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
