import {Directive, Input} from "@angular/core";

export interface DataType {
  alignment: Alignment;
}

export interface DataTypes {
  [index: string]: DataType;
}

export type Alignment = "left" | "center" | "right";

const datatypes: DataTypes = {
  amount: {
    alignment: "right",
  },
  percent2: {
    alignment: "right",
  },
  date: {
    alignment: "left",
  },
};

@Directive({
  selector: "[datatype]",
})
export class DatatypeDirective {
  @Input("datatype") name: string;

  get datatype(): DataType | void {
    return datatypes[this.name] || this.warnAboutUnkownDatatype();
  }

  get alignment() {
    return this.datatype?.alignment;
  }

  private warnAboutUnkownDatatype(): void {
    console.error(
      "Datatype \"%s\" does not have a configuration in this directive.\n" +
      "Most likely, there's a typo in the name, so check your spelling.\n" +
      "If it's not a typo, go to the this file and provide all necessary information about this new datatype.",
      this.name,
    );
  }
}
