import {DataSource} from "@angular/cdk/collections";
import {Component, OnInit} from "@angular/core";
import {PageEvent} from "@angular/material/paginator";
import {MatSlideToggleChange} from "@angular/material/slide-toggle";
import {BehaviorSubject, combineLatest, concat, identity, noop, Observable, of, UnaryFunction} from "rxjs";
import {catchError, first, map, switchMap, tap} from "rxjs/operators";
import {TransactionService} from "../services/api/transactions/transaction.service";
import {Company} from "../services/api/transactions/dtos/company";

interface PaginatedResult<T> {
  content: T[];
  total: number;
}

interface Sorting {
  freeFormSearch: string;
}

export interface GenericColumn<RowData> {
  header: string;
}

export function fromProp<T>(prop: string, transform: UnaryFunction<any, T> = identity): UnaryFunction<any, T> {
  return anObject => transform(anObject[prop]);
}

export function toCountryName(countryCode: string): string {
  switch (countryCode) {
    case "CRI":
      return "Costa Rica";
    case "DEU":
      return "Germany";
    case "EST":
      return "Estonia";
    default:
      return "Unknown";
  }
}

class BackendDataSource implements DataSource<Company> {
  public isLoading$ = new BehaviorSubject<boolean>(true);
  public totalRows$ = new BehaviorSubject<number>(0);

  refresh$ = new BehaviorSubject<void>(undefined);
  emptyData: Company[] = [];

  constructor(
    private pagination$: Observable<PageEvent>,
    private sorting$: Observable<Sorting>,
    private backend: TransactionService,
  ) {
  }

  connect(): Observable<Company[]> {
    const initialState$ = of(this.emptyData);

    const backendCallTriggers$ = combineLatest([
      this.pagination$,
      this.sorting$,
      this.refresh$,
    ]);

    return concat(
      initialState$,
      backendCallTriggers$.pipe(
        tap(_ => this.loading()),
        switchMap(settings => this.callBackend(settings[0], settings[1])
          .pipe(first())),
        tap(responseBody => this.totalRows$.next(responseBody.total)),
        map(responseBody => responseBody.content),
        tap(_ => this.notLoading(), () => this.notLoading(), noop),
      ),
    );
  }

  disconnect(): void {
    // Do nothing for now. I'll later check if it's necessary to disconnect gracefully from the database.
  }

  refresh() {
    this.refresh$.next();
  }

  private loading() {
    this.isLoading$.next(true);
  }

  private notLoading() {
    this.isLoading$.next(false);
  }

  private callBackend(pagination: PageEvent, sorting: Sorting): Observable<PaginatedResult<Company>> {
    return this.backend
      .listCompanies()
      .pipe(
        catchError(err => {
          console.error(err);
          return [];
        }),
        map(data => {
          return {
            content: data,
            total: data.length,
          };
        }),
      );
  }
}

@Component({
  selector: "app-generic-table",
  templateUrl: "./generic-table.component.html",
  styleUrls: ["./generic-table.component.sass"],
})
export class GenericTableComponent implements OnInit {
  paginationEvents$ = new BehaviorSubject<PageEvent>({
    length: 0,
    pageIndex: 0,
    pageSize: 20,
  });

  sortingEvents$ = new BehaviorSubject<Sorting>({
    freeFormSearch: "",
  });

  columns = {
    name: {
      header: "Name",
    },
    address: {
      header: "Address",
    },
    iban: {
      header: "IBAN",
    },
  };

  sampleDataSource: BackendDataSource;
  displayedColumns: string[];
  minimizeAddress = false;

  constructor(private backend: TransactionService) {
  }

  ngOnInit(): void {
    this.displayedColumns = Object.getOwnPropertyNames(this.columns);
    this.sampleDataSource = new BackendDataSource(this.paginationEvents$, this.sortingEvents$, this.backend);
  }

  updatePagination(event: PageEvent) {
    this.paginationEvents$.next(event);
  }

  switchFullAddress(event: MatSlideToggleChange) {
    this.minimizeAddress = event.checked;
  }
}
