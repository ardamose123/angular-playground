import {Directive, ElementRef, Optional} from "@angular/core";
import {MatCell, MatHeaderCell} from "@angular/material/table";
import {CdkColumnDef} from "@angular/cdk/table";
import {Alignment, DatatypeDirective} from "./datatype.directive";

function addAlignment(elementRef: ElementRef, alignment: Alignment | undefined) {
  if (alignment) {
    elementRef.nativeElement.classList.add("align-" + alignment);
  }
}

@Directive({
  selector: "app-cell, td[app-cell]",
  host: {
    "class": "app-cell",
    "role": "gridcell",
  },
})
export class CellDirective extends MatCell {
  constructor(
    columnDef: CdkColumnDef,
    elementRef: ElementRef,
    @Optional() datatype: DatatypeDirective,
  ) {
    super(columnDef, elementRef);
    addAlignment(elementRef, datatype?.alignment);
  }
}

@Directive({
  selector: "app-header-cell, th[app-header-cell]",
  host: {
    "class": "app-header-cell",
    "role": "columnheader",
  },
})
export class HeaderCellDirective extends MatHeaderCell {
  constructor(
    columnDef: CdkColumnDef,
    elementRef: ElementRef,
    @Optional() datatype: DatatypeDirective,
  ) {
    super(columnDef, elementRef);
    addAlignment(elementRef, datatype?.alignment);
  }
}
