import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatTableModule} from "@angular/material/table";
import {DataTypeRenderingModule} from "../data-type-rendering/data-type-rendering.module";
import {CellDirective, HeaderCellDirective} from "./cell.directive";
import {DatatypeDirective} from "./datatype.directive";
import {GenericTableComponent} from "./generic-table.component";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [GenericTableComponent, DatatypeDirective, CellDirective, HeaderCellDirective],
  imports: [
    CommonModule,
    MatTableModule,
    DataTypeRenderingModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatPaginatorModule,
    MatIconModule,
    MatSlideToggleModule,
    HttpClientModule,
  ],
  exports: [GenericTableComponent],
})
export class GenericTableModule {
}
